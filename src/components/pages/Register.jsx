// import { useState, useEffect, useContext } from 'react';
// import { Container, Form, Button } from 'react-bootstrap';
// import { Redirect, useHistory } from 'react-router-dom';
// import UserContext from '../UserContext';
// import Swal from 'sweetalert2';

// export default function Register(){

//     const {user} = useContext(UserContext);
//     const history = useHistory();

//     // State hooks to store the values of the input fields
//     const [email, setEmail] = useState('');
//     const [password1, setPassword1] = useState('');
//     const [password2, setPassword2] = useState('');

//     // State to determine whether submit button is enabled or not
//     const [isActive, setIsActive] = useState(false);

//     // Function to simulate user registration
//     function registerUser(e) {
//         // Prevents page redirection via form submission
//         e.preventDefault();
//         fetch('http://localhost:4000/api/users/checkEmail', {
//             method: "POST",
//             headers: {
//                 'Content-Type': 'application/json'
//             },
//             body: JSON.stringify({
//                 email: email
//             })
//         })
//         .then(res => res.json())
//         .then(data => {

//             console.log(data);

//             if(data === true){

//                 Swal.fire({
//                     title: 'Duplicate email found',
//                     icon: 'error',
//                     text: 'Kindly provide another email to complete the registration.'  
//                 });

//             }
//             else{
//                 fetch('http://localhost:4000/api/users/register', {
//                     method: "POST",
//                     headers: {
//                         'Content-Type': 'application/json'
//                     },
//                     body: JSON.stringify({
//                         email: email,
//                         mobileNo: mobileNo,
//                         password: password1
//                     })

//                 })
//                 .then(res => res.json())
//                 .then(data => {
//                     console.log(data);

//                     if (data === true) {

//                         // Clear input fieldd
//                         setEmail('');
//                         setPassword1('');
//                         setPassword2('');

//                         Swal.fire({
//                             title: 'Registration successful',
//                             icon: 'success',
//                             text: 'Welcome to SHLD!'
//                         });

//                         history.push("/login");

//                     } else {

//                         Swal.fire({
//                             title: 'Something wrong',
//                             icon: 'error',
//                             text: 'Please try again.'   
//                         });

//                     }
//                 })
//             }


//         })

//     }

//     useEffect(() => {
//         // Validation to enable the submit buttion when all fields are populated and both passwords match
//         if(( email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
//             setIsActive(true);
//         }
//         else{
//             setIsActive(false);
//         }
//     }, [email, password1, password2])

//     return (
//         (user.id !== null) ?
//             <Redirect to="/courses" />
//         :
//         <Container>
//             <h1>Register</h1>
//             <Form className="mt-3" onSubmit={(e) => registerUser(e)}>

//               <Form.Group className="mb-3" controlId="userEmail">
//                 <Form.Label>Email address</Form.Label>
//                 <Form.Control 
//                     type="email" 
//                     placeholder="Enter email" 
//                     value = {email}
//                     onChange = { e => setEmail(e.target.value)}
//                     required 
//                 />
//                 <Form.Text className="text-muted">
//                   We'll never share your email with anyone else.
//                 </Form.Text>
//               </Form.Group>

//               <Form.Group className="mb-3" controlId="password1">
//                 <Form.Label>Password</Form.Label>
//                 <Form.Control 
//                     type="password" 
//                     placeholder="Password" 
//                     value={password1}
//                     onChange = { e => setPassword1(e.target.value)}
//                     required 
//                 />
//               </Form.Group>
              
//               <Form.Group className="mb-3" controlId="password2">
//                 <Form.Label>Verify Password</Form.Label>
//                 <Form.Control 
//                     type="password" 
//                     placeholder="Verify Password" 
//                     value={password2}
//                     onChange = { e => setPassword2(e.target.value)}
//                     required 
//                 />
//               </Form.Group>
//             {/* Conditionally render submit button based on isActive state */}
//               { isActive ? 
//                     <Button variant="primary" type="submit" id="submitBtn">
//                       Submit
//                     </Button>
//                 :
//                     <Button variant="primary" type="submit" id="submitBtn" disabled>
//                       Submit
//                     </Button>
//               }
              
//             </Form>
//         </Container>
//     )
// }
